import React, { useState, useContext, useEffect } from 'react'

import Header from '../components/Header'
import EventCard from '../components/EventCard'
import EventsContext from '../contexts/EventsContext'

const MyEventsPage = () => {
  const { eventsList, myEvents } = useContext(EventsContext)
  const [events, setEvents] = useState([])

  useEffect(() => {
    const filteredEvents = eventsList.filter(event => myEvents.includes(event.id))
    setEvents([...filteredEvents])
  }, [eventsList, myEvents])

  const renderNoEventsMessage = () => {
    return (
      <div className="m-auto">
        <p className="text-2xl">You haven't signed up for any events yet!</p>
      </div>
    )
  }

  const renderEvents = () => {
    return events.map(event => <EventCard key={event.id} event={event} />)
  }

  return (
    <div className="h-screen">
      <Header />
      <div className="flex flex-col items-center">
        <div className="mt-4 p-4 flex flex-col items-center">
          <p className="text-4xl font-boldk">My Events</p>
          <p className="text-sm">{events.length} events</p>
        </div>
        <div className="w-1/3">
        </div>
        <div className="flex flex-wrap w-5/6 justify-around">
          { events.length > 0 ? renderEvents() : renderNoEventsMessage() }
        </div>
      </div>
    </div>
  )
}

export default MyEventsPage
