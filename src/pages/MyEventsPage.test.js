import React, { useState, useContext } from 'react'
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom'

import MyEventsPage from './MyEventsPage'
import EventsContext from '../contexts/EventsContext'

import TestEventsData from '../tests/testEvents.json'

test('renders empty My Events page when myEvents is empty', () => {
  const { queryByTestId, getByText } = render(
    <EventsContext.Provider value={{ eventsList: TestEventsData, myEvents: [] }}>
      <BrowserRouter>
        <MyEventsPage />
      </BrowserRouter>
    </EventsContext.Provider>
  );
  const text = getByText(/You haven't signed up for any events yet!/i);
  expect(queryByTestId('event-card')).toBeNull()
  expect(text).toBeInTheDocument();
});

test('renders Event cards when event data is available', () => {
  const testEvent = TestEventsData[0]
  const { getAllByTestId, getByText } = render(
    <EventsContext.Provider value={{ eventsList: TestEventsData, myEvents: [testEvent.id]}}>
      <BrowserRouter>
        <MyEventsPage />
      </BrowserRouter>
    </EventsContext.Provider>
  );
  const eventCards = getAllByTestId("event-card")
  const text = getByText(testEvent.name);
  expect(eventCards).toHaveLength(1);
  expect(text).toBeInTheDocument();
});
