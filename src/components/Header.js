import React from 'react'
import Navbar from './Navbar'

import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <div className="flex bg-green-800 items-center px-6 shadow-lg">
      <Link to="/" className="py-2 px-4">
        <p className="text-2xl text-yellow-400 font-black">Serve The World</p>
      </Link>
      <Navbar />
    </div>
  )
}

export default Header
