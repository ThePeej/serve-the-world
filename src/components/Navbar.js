import React, { useContext }from 'react'
import { Link } from 'react-router-dom'

import EventsContext from '../contexts/EventsContext'

const Navbar = () => {
  const { myEvents } = useContext(EventsContext)

  return (
    <nav className="flex justify-between flex-grow items-center">
      <Link to="/events" className="p-4 text-white text-xl font-medium hover:underline">Events</Link>
      <Link to="/my-events" className="p-4 text-white text-xl font-medium hover:underline">My Events({myEvents.length})</Link>
    </nav>
  )
}

export default Navbar
