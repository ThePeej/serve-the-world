import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom'
import LandingPage from './LandingPage';


test('renders landing page text', () => {
  const { getByText } = render(
    <BrowserRouter>
      <LandingPage />
    </BrowserRouter>
  );
  const text = getByText(/serve the world/i);
  expect(text).toBeInTheDocument();
});
