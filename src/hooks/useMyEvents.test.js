import { renderHook, act } from '@testing-library/react-hooks'
import useMyEvents from './useMyEvents'

import TestEventsData from '../tests/testEvents.json'

test('should add event to My Events', () => {
  const { result } = renderHook(() => useMyEvents())
  const testEvent = TestEventsData[0]

  act(() => {
    result.current.addEventToMyEvents(testEvent.id)
  })

  expect(result.current.myEvents).toContain(testEvent.id)
})

test('should remove event from My Events', () => {
  const { result } = renderHook(() => useMyEvents())
  const testEvent = TestEventsData[0]

  result.current.myEvents = [testEvent.id]
  expect(result.current.myEvents).toContain(testEvent.id)

  act(() => {
    result.current.removeEventFromMyEvents(testEvent.id)
  })
  expect(result.current.myEvents).not.toContain(testEvent.id)
})
