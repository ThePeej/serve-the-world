import React, { useState, useContext } from 'react'
import { render } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom'

import EventShowPage from './EventShowPage'
import EventsContext from '../contexts/EventsContext'

import TestEventsData from '../tests/testEvents.json'

test.skip('renders empty Event Show page when no event data is found', () => {
  const { getByText } = render(
    <EventsContext.Provider value={{ eventsList: TestEventsData }}>
      <MemoryRouter initialEntries={["/events/100"]}>
        <Route path="/users/:id">
          <EventShowPage />
        </Route>
      </MemoryRouter>
    </EventsContext.Provider>
  );
  const text = getByText(/event could not be found/i);
  expect(text).toBeInTheDocument();
});
