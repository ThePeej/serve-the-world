import React, { useContext }from 'react'
import { Link } from 'react-router-dom'

import EventsContext from '../contexts/EventsContext'

const EventCard = ({ event }) => {
  const { myEvents } = useContext(EventsContext)

  const renderButton = () => {
    let buttonText = "Learn More"
    let buttonColorClass = "bg-green-700 text-yellow-400"
    if (myEvents.includes(event.id)) {
      buttonText = "Can't wait!"
      buttonColorClass = "text-green-800 bg-yellow-400"
    }

    return <Link to={`/events/${event.id}`} className={"border border-gray-800 rounded-full p-2 mb-2 mt-4 w-1/2 font-semibold text-center self-center " + buttonColorClass}>{buttonText}</Link>
  }

  return (
    <div className="w-1/3 p-4 h-1/2" data-testid="event-card"> 
      <div className="bg-white border border-gray-600 rounded px-4 py-2 flex flex-col h-full">
        <img src={event.img} alt={event.name} />
        <p className="text-xl font-semibold underline mt-2">{event.name}</p>
        <p className="text-lg">Volunteers still needed: {event.volunteersNeeded - event.volunteers}</p>
        <p className="text-sm">
          <span className="font-semibold">
            Location:
          </span>
          {" " + event.address.street}, {event.address.city}, {event.address.state}
        </p>
        <p className="text-sm">
          <span className="font-semibold">
            Date:
          </span>
          {" " + event.date} at {event.time}
        </p>
        { renderButton() }
      </div>
    </div>
  )
}

export default EventCard
