import React, { useState, useContext } from 'react'
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom'

import EventsIndexPage from './EventsIndexPage'
import EventsContext from '../contexts/EventsContext'

import TestEventsData from '../tests/testEvents.json'

test('renders empty Events Index page when no event data is available', () => {
  const { queryByTestId, getByText } = render(
    <EventsContext.Provider value={{ eventsList: [], myEvents: [] }}>
      <BrowserRouter>
        <EventsIndexPage />
      </BrowserRouter>
    </EventsContext.Provider>
  );
  const text = getByText(/no events/i);
  expect(queryByTestId('event-card')).toBeNull()
  expect(text).toBeInTheDocument();
});

test('renders Event cards when event data is available', () => {
  const { getAllByTestId, getByText } = render(
    <EventsContext.Provider value={{ eventsList: TestEventsData, myEvents: []}}>
      <BrowserRouter>
        <EventsIndexPage />
      </BrowserRouter>
    </EventsContext.Provider>
  );
  const eventCards = getAllByTestId("event-card")
  expect(eventCards).toHaveLength(7);
});
