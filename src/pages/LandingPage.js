import React from 'react'

import { Link } from 'react-router-dom'

const LandingPage = () => {
  return (
    <div className="h-screen flex flex-col items-center justify-center bg-cover bg-center" style={{ backgroundImage: "url('https://images.unsplash.com/photo-1525026198548-4baa812f1183?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9')" }}>
      <p className="text-4xl text-white text-center text-yellow-200 my-6">“Small acts, when multiplied by millions of people, can transform the world.”</p>
      <Link to="/events" className="border-2 border-solid rounded border-gray-600 px-8 bg-green-800 shadow-lg hover:shadow-xl">
        <p className="text-4xl text-yellow-400 font-semibold">Serve the World</p>
      </Link>
    </div>
  )
}

export default LandingPage
