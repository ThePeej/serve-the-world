import React, { useState, useContext, useEffect } from 'react'
import Select from 'react-select'

import Header from '../components/Header'
import EventCard from '../components/EventCard'
import EventsContext from '../contexts/EventsContext'

const EventsIndexPage = () => {
  const { eventsList } = useContext(EventsContext)
  const [events, setEvents] = useState([])
  const [categoryFilter, setCategoryFilter] = useState("")

  useEffect(() => {
    setEvents(eventsList)
  }, [eventsList])

  const renderNoEventsMessage = () => {
    return (
      <div className="m-auto">
        <p className="text-2xl">No Events</p>
      </div>
    )
  }

  const handleCategoryFilterChange = (option) => {
    if (option.value === "") {
      setCategoryFilter(option)
      return setEvents([...eventsList])
    }
    const categoryFilter = option.value
    const filteredEvents = eventsList.filter(event => event.categories.includes(categoryFilter))
    setCategoryFilter(option)
    setEvents([...filteredEvents])
  }

  const renderCategoryFilter = () => {
    const categories = eventsList.map(event => event.categories).flat()
    const uniqueCategories = [...new Set(categories)].sort()
    const categoryFilterOptions = uniqueCategories.map(category => {
      const categoryLabel = category.charAt(0).toUpperCase() + category.slice(1);

      return (
        { label: categoryLabel, value: category }
      )
    })

    const noneOption = { label: "", value: "" }

    return (
      <Select
        name="Category"
        placeholder="Filter by event category"
        value={categoryFilter}
        onChange={handleCategoryFilterChange}
        options={[noneOption, ...categoryFilterOptions]}
      />
    )
  }

  const renderEvents = () => {
    return events.map(event => <EventCard key={event.id} event={event} />)
  }

  return (
    <div className="h-full">
      <Header />
      <div className="flex flex-col items-center">
        <div className="mt-4 p-4 flex flex-col items-center">
          <p className="text-4xl font-boldk">Events</p>
          <p className="text-sm">{events.length} events</p>
        </div>
        <div className="w-1/3">
          {renderCategoryFilter()}
        </div>
        <div className="flex flex-wrap w-5/6 justify-around">
          { events.length > 0 ? renderEvents() : renderNoEventsMessage() }
        </div>
      </div>
    </div>
  )
}

export default EventsIndexPage
