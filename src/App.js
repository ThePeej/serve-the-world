import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import LandingPage from './pages/LandingPage'
import EventsIndexPage from './pages/EventsIndexPage'
import EventShowPage from './pages/EventShowPage'
import MyEventsPage from './pages/MyEventsPage'

import useMyEvents from './hooks/useMyEvents'
import EventsContext from './contexts/EventsContext'

import './styles.css';

const App = () => {
  const [eventsList, setEventsList] = useState([])
  const { myEvents, addEventToMyEvents, removeEventFromMyEvents } = useMyEvents()

  async function fetchData() {
    const response = await fetch("https://my-json-server.typicode.com/thepeej/mock-json-api/events");
    const data = await response.json();
    setEventsList(data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <EventsContext.Provider value={{
      eventsList: eventsList,
      myEvents: myEvents,
      addEventToMyEvents: addEventToMyEvents,
      removeEventFromMyEvents: removeEventFromMyEvents
    }}>
      <BrowserRouter>
        <div className="bg-yellow-100">
          <Switch>
            <Route path="/" component={LandingPage} exact />
            <Route path="/events" component={EventsIndexPage} exact />
            <Route path="/events/:id" component={EventShowPage} exact />
            <Route path="/my-events" component={MyEventsPage} exact />
          </Switch>
        </div>
      </BrowserRouter>
    </EventsContext.Provider>
  );
}

export default App;
