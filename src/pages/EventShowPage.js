import React, { useState, useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'

import EventsContext from '../contexts/EventsContext'

import Header from '../components/Header'

const EventsShowPage = () => {
  const [event, setEvent] = useState(null)

  const { eventsList, myEvents, addEventToMyEvents, removeEventFromMyEvents } = useContext(EventsContext)

  const { id } = useParams()

  useEffect(() => {
    setEvent(eventsList.find(event => event.id === parseInt(id)))
  }, [eventsList, id])

  const renderAddOrRemoveEvent = () => {
    return myEvents.includes(event.id)
      ?
      <button onClick={() => removeEventFromMyEvents(event.id)} className="border-2 rounded-lg p-4 w-1/3 text-xl font-bold mb-6 text-green-800 bg-yellow-400 shadow-xl hover:underline">I can't make it anymore</button>
      :
      <button onClick={() => addEventToMyEvents(event.id)}className="border-2 rounded-lg p-4 w-1/3 text-xl font-bold mb-6 bg-green-800 text-yellow-400 shadow-xl hover:underline">Sign me up!</button>
  }

  const renderNoEventMessage = () => {
    return (
      <div className="m-auto">
        <p className="text-2xl">Event could not be found</p>
      </div>
    )
  }

  const renderEventDetails = () => {
    return (
      <div className="m-auto flex flex-col items-center w-5/6 p-10">
        <div className="flex flex-col m-6 items-center">
          <p className="text-4xl font-bold mt-4">{event.name}</p>
          <p className="text-lg font-semibold">{" " + event.date} at {event.time}</p>
          <p className="text-lg font-semibold">{ event.address.street}, {event.address.city}, {event.address.state}</p>
        </div>
        { renderAddOrRemoveEvent() }
        <img src={event.img} alt={event.name} className="w-3/5"/>
        <div className="my-4">
          <p className="font-semibold underline">Description</p>
          <p className="text-sm">{event.description}</p>
        </div>
        <div className="flex justify-around w-full items-center m-6">
          <div className="flex flex-col">
            <p>Number of volunteers: {event.volunteers}</p>
            <p>Volunteers still needed: {event.volunteersNeeded - event.volunteers}</p>
          </div>
          <div className="flex flex-col items-center">
            <p>Contact: {event.contact}</p>
            <a href={event.website} target="_blank" rel="noopener noreferrer" className="border border-gray-800 rounded text-lg px-4">Website</a>
          </div>
          <div>
            <p className="font-semibold underline">Categories</p>
            <ul>
              {event.categories.map(c => <li>{c}</li>)}
            </ul>
          </div>
        </div>
      </div>
    )
  }
  return (
    <div className="h-full">
      <Header />
      { !!event ? renderEventDetails() : renderNoEventMessage() }
    </div>
  )
}

export default EventsShowPage
