import { useState } from 'react'

const useMyEvents = () => {
  const [myEvents, setMyEvents] = useState([])

  const addEventToMyEvents = (id) => {
    if (!myEvents.includes(id)) {
      setMyEvents([...myEvents, id])
    }
  }

  const removeEventFromMyEvents = (id) => {
    const updatedMyEvents = myEvents.filter(eventId => eventId !== id)
    setMyEvents([...updatedMyEvents])
  }

  return { myEvents, addEventToMyEvents, removeEventFromMyEvents }
}

export default useMyEvents
